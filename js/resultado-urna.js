var divComponenteAlert = document.querySelector("#idAlertErro");
var tableBody = document.querySelector("#idTableBody");
var apuracaoVotos = [];

// AO CARREGAR A TELA
window.onload = async function () {

    let carregamento = exibirCarregamento("Carregando apuração dos votos");
    exibirMensagem(carregamento, "alert-info", false)
    apuracaoVotos = await getApuracao();
    console.log(apuracaoVotos);

    if (apuracaoVotos.length == 0) {
        return;
    }

    removerComponentesInternos(divComponenteAlert);

    exibirResultadoApuracao(apuracaoVotos);

}

// OBTER A MATRIZ DE VOTOS ATRAVÉS DO SERVIDOR
async function getApuracao() {
    try {
        var response = await fetch("http://localhost:3001/apuracao");
        console.log(response);
    } catch (error) {
        removerComponentesInternos(divComponenteAlert);
        exibirMensagem("<b>Não foi possível obter a apuração de votos.</b><br>Consulte o administrador do sistema.", "alert-danger", true);
        console.log("Erro ao carregar a apuração de votos (verifique se o servidor está disponível) --- " + error);
        return [];
    }

    return response.json();
}

function exibirResultadoApuracao(apuracaoVotos) {
    for (let i = 0; i < apuracaoVotos.length; i++) {
        const resultadoCand = apuracaoVotos[i];
        criarLinha(resultadoCand, i);
    }
}

function criarLinha(resultadoCand, i) {
    let novaLinha = document.createElement("tr");
    novaLinha.setAttribute("id", "idLinha" + (i + 1));

    let coluna1 = document.createElement("th");
    coluna1.setAttribute("class", "clNumeracaoLinha" + (i + 1));
    coluna1.innerHTML = `<span class="fs-5">${(i + 1)}</span>`;

    let coluna2 = document.createElement("td");

    let imgCand;

    imgCand =
        `<img src="${resultadoCand[2]}" id="idImagemCand${i + 1}"
                class="img-fluid img-thumbnail rounded mx-auto d-block" alt="Imagem do(a) candidato(a) ${resultadoCand[1]}"
                height="10" style="width: 150px;">`


    coluna2.innerHTML = imgCand;

    let coluna3 = document.createElement("td");
    coluna3.innerHTML = `<span class="fs-5">${resultadoCand[0]}</span>`;

    let coluna4 = document.createElement("td");
    coluna4.innerHTML = `<span class="fs-5">${resultadoCand[1]}</span>`;

    let coluna5 = document.createElement("td");
    coluna5.innerHTML = `<span class="badge bg-info text-dark fs-2">${resultadoCand[3]}</span>`;

    novaLinha.appendChild(coluna1);

    novaLinha.appendChild(coluna2);
    novaLinha.appendChild(coluna3);
    novaLinha.appendChild(coluna4);
    novaLinha.appendChild(coluna5);

    tableBody.appendChild(novaLinha);
}

function removerComponentesInternos(father) {
    while (father.firstChild) {
        father.removeChild(father.firstChild);
    }
}

function exibirMensagem(mensagem, corCaixa, contemBotao) {
    let alert = document.createElement("div")
    alert.className = "alert alert-dismissible fade show shadow-sm"
    alert.classList.add(corCaixa);
    alert.role = "alert";
    alert.innerHTML = `${mensagem}`
    divComponenteAlert.appendChild(alert);

    if (contemBotao) {
        var closeBt = document.createElement("button");
        closeBt.type = "button"
        closeBt.classList.add("btn-close")
        closeBt.setAttribute("data-bs-dismiss", "alert")
        closeBt.setAttribute("aria-label", "Close")
        alert.appendChild(closeBt);
    }
}

function exibirCarregamento(mensagem) {
    let conteudoCarregamento = `<div class="d-flex align-items-center">
                                    <strong>${mensagem}...</strong>
                                <div class="spinner-border ms-auto" role="status" aria-hidden="true"></div>
                                </div> `

    return conteudoCarregamento;
}